from flask import Flask, request, jsonify
import pickle

import numpy as np
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import jaccard_similarity_score

NUM_WORDS = 10
NUM_OUTPUT_CLASS = 2
TEST_SIZE = 0.2
SEED = 42
NUM_EPOCHS = 10
BATCH_SIZE = 8196

# Paths
MODELS_PATH = "./models/"

# load the model into memory initially
# model_path = MODELS_PATH + "baseline.h5"
model_path = MODELS_PATH + "baselineh5.gz"
similarity_model = load_model(model_path)


def load_tokenizer(tokenizer_path):
    try:
        with open(tokenizer_path, 'rb') as handle:
            return pickle.load(handle)
    except:
        raise Exception("Failed to load tokenizer")


def prediction(input_sentence1, input_sentence2):
    try:
        tokenizer_path = MODELS_PATH + "sentence_tokenizer.pkl"
    except FileNotFoundError:
        raise Exception("Keras Tokenizer not found")
    tokenizer = load_tokenizer(tokenizer_path)
    sentence1 = tokenizer.texts_to_sequences([input_sentence1])
    sentence2 = tokenizer.texts_to_sequences([input_sentence2])
    padded_s1 = pad_sequences(sentence1, NUM_WORDS)
    padded_s2 = pad_sequences(sentence2, NUM_WORDS)
    print(padded_s1)
    print(padded_s2)
    try:
        neural_score = similarity_model.predict([padded_s1, padded_s2])[0][0]
    except:
        raise("Neural Prediction Failed")
    try:
        flattened_padded_s1 = np.array(padded_s1).flatten()
        flattened_padded_s2 = np.array(padded_s2).flatten()
        print(flattened_padded_s1)
        print(flattened_padded_s2)
        jaccard_score = jaccard_similarity_score(flattened_padded_s1, flattened_padded_s2)
        # jaccard_score = float(0.8678)
    except:
        raise("Jaccard Prediction Failed")
    return neural_score, jaccard_score


# Initialize the Flask application
app = Flask(__name__)


def _one_digit_roundoff(score):
    return float(round(score * 100, 1))


def _get_score(data):
    sentence1 = str(data.get('sentence1')).strip()
    sentence2 = str(data.get('sentence2')).strip()
    neural_score, jaccard_score = prediction(sentence1, sentence2)
    return _one_digit_roundoff(neural_score), _one_digit_roundoff(jaccard_score)


@app.route('/predict', methods=['POST'])
def index():
    if request.method == "POST":
        post_data = request.get_json()
        neural_score, jaccard_score = _get_score(post_data)
    # Return a json object to UI
    return jsonify({"neural_score": neural_score, "jaccard_score": jaccard_score})

if __name__ == '__main__':
    app.run(debug=True)
