import os

src_dir = os.path.dirname(__file__)
models_dir = os.path.join(src_dir, os.pardir)

NUM_WORDS = 10
NUM_OUTPUT_CLASS = 2
TEST_SIZE = 0.2
SEED = 42
NUM_EPOCHS = 10
BATCH_SIZE = 8196