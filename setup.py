from setuptools import setup


if __name__ == "__main__":
    setup(
        name="sentence-similarity-herokuapp",
        version="0.1",
        description="Sentence Similarity",
        url="https://gitlab.com/janardhanpshetty/sentence-similarity-herokuapp",
        author="Janardhan Shetty",
        author_email="janardhanp22@gmail.com",
        install_requires=[ 'keras', 'tensorflow', 'pandas', 'numpy']
    )